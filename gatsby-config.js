module.exports = {
  pathPrefix: "/",
  siteMetadata: {
    title: "rebecca",
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    "@chakra-ui/gatsby-plugin",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `src/images/favicon.svg`,
        icon_options: {
          purpose: `maskable`,
        },
      },
    },
  ],
};
