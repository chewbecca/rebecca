import * as React from "react";
import {
  VStack,
  HStack,
  Text,
  Heading,
  Box,
  Link,
  Flex,
} from "@chakra-ui/react";
import ImmaMess from "../components/imma-mess";
import ProjectCard from "../components/project-card";
import Footer from "../components/footer";
import Layout from "../components/layout";
import theme from "../theme/theme";
import { jbSprachen, mergeMerch } from "../assets/project-svgs";
import { useEffect, useState } from "react";
import SocialLink from "../components/social-link";
import { IoLogoTwitter, IoLogoGithub, IoLogoGitlab } from "react-icons/io";
import { SiGitlab } from "react-icons/si";
import { getColor } from "@chakra-ui/theme-tools";
import { Helmet } from "react-helmet";

// markup
function IndexPage() {
  const [screenHeight, setScreenHeight] = useState("760");

  useEffect(() => {
    setScreenHeight(window.screen.height);
  });

  return (
    <Layout title="Rebecca - Home">
      <ImmaMess></ImmaMess>
      <VStack alignItems="start" spacing="8">
        <VStack
          h={["lg", "lg", "lg", `${screenHeight}px`]}
          alignItems="start"
          justifyContent="center"
        >
          <Text
            color="pink.400"
            fontSize="32px"
            lineHeight="40px"
            fontWeight="400"
            mb="2"
          >
            Web Developer and UX Designer
          </Text>
          <Heading
            color="yellow.400"
            fontSize={["48px", "72px"]}
            lineHeight={["52px", "85px"]}
            fontWeight="700"
            mb="5"
          >
            Rebecca Bettinger
          </Heading>
          <HStack spacing="4">
            <Link
              href="https://twitter.com"
              target="_blank"
              color="twitter.500"
            >
              <IoLogoTwitter size="2rem"></IoLogoTwitter>
            </Link>
            <Link href="https://github.com" target="_blank" color="#9f7be1">
              <IoLogoGithub size="2rem"></IoLogoGithub>
            </Link>
            <Link href="https://gitlab.com" target="_blank" color="#e24329">
              <SiGitlab size="2rem"></SiGitlab>
            </Link>
          </HStack>
        </VStack>
        <VStack alignItems="start" w="100%" spacing="4">
          <Text
            color="pink.400"
            fontSize="32px"
            lineHeight="40px"
            fontWeight="400"
          >
            Projects
          </Text>
          <Flex wrap="wrap" alignItems="center" justifyContent="center">
            <ProjectCard
              title="mergemerch.com"
              description="UI Design and Frontend Development"
              svg={mergeMerch()}
              url="https://mergemerch.com"
            ></ProjectCard>
            <Box m={["0", "0", "4"]} w="0" h="0"></Box>
            <ProjectCard
              title="jb-sprachen.de"
              description="UI Design and Frontend Development"
              svg={jbSprachen()}
              url="https://jb-sprachen.de"
            ></ProjectCard>
          </Flex>
        </VStack>
      </VStack>
    </Layout>
  );
}

export default IndexPage;
