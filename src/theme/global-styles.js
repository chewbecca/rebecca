export const styles = {
  global: () => ({
    body: {
      backgroundColor: "gray.900",
      maxW: "1440px",
      width: "100%",
    },
    "h1, h2, h3, h4": {
      color: "yellow.400",
      fontFamily: "Rubik",
      fontWheight: "700",
    },
    h1: {
      fontSize: "2xl",
      pb: "6",
    },
    h2: {
      fontSize: "xl",
      pb: "4",
    },
    h3: {
      fontSize: "lg",
      pb: "2",
    },
    "p, span, a, li": {
      color: "pink.400",
      fontFamily: "Ibarra Real Nova",
    },
    "p, ul": {
      pb: "4",
    },
    ul: {
      pl: "6",
    },
  }),
};
