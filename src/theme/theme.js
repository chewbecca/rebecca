import { extendTheme } from "@chakra-ui/react";
import { styles } from "./global-styles";
import "@fontsource/rubik/700.css";
import "@fontsource/ibarra-real-nova";

const theme = extendTheme({
  fonts: {
    heading: "Rubik",
    body: "Ibarra Real Nova",
  },
  styles,
});

export default theme;
