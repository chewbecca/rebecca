import * as React from "react";
import {
  VStack,
  Text,
  Heading,
  Box,
  ChakraProvider,
  keyframes,
  Link,
  HStack,
  BreadcrumbSeparator,
} from "@chakra-ui/react";
import { Link as gLink } from "gatsby";

const Footer = () => {
  return (
    <Box maxW="1440px" w="100%" height="48" pt="24" m="0">
      <svg width="100%" height="2rem">
        <g fill="none" fillRule="evenodd">
          <g
            transform="translate(-89.000000, -1853.000000)"
            stroke="#D53F8C"
            strokeWidth="2"
          >
            <path d="M89.3284869,1868.63758 C214.139551,1870.81617 93.2623117,1869.77105 337.141903,1860.73137 C351.885656,1860.18488 366.667041,1861.33053 381.403372,1860.61121 C407.070112,1859.35835 432.580576,1854.98637 458.277327,1854.81846 C1130.5128,1850.42589 982.066944,1869.10104 1104.72252,1869.98506 C1183.00803,1870.5493 1258.78642,1858.90889 1336.80446,1865.74614"></path>
          </g>
        </g>
      </svg>
      <VStack py="16" alignItems="start">
        <HStack>
          <Link
            as={gLink}
            color="yellow.400"
            fontFamily="Rubik"
            fontWeight="700"
            to="/legal-notice/"
          >
            Legal Notice
          </Link>
          <Box width="2" height="2" backgroundColor="pink.400">
            {" "}
          </Box>
          <Link
            as={gLink}
            color="yellow.400"
            fontFamily="Rubik"
            fontWeight="700"
            to="/privacy-policy/"
          >
            Privacy Policy
          </Link>
        </HStack>
        <Text color="pink.400">
          made with ❤️ using react, gatsby and chakra-ui
        </Text>
      </VStack>
    </Box>
  );
};

export default Footer;
