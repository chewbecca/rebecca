import * as React from "react";
import {
  VStack,
  Text,
  Heading,
  Box,
  Image,
  ChakraProvider,
  keyframes,
  Flex,
} from "@chakra-ui/react";
import { IoMdOpen } from "react-icons/io";
import { Link } from "gatsby";

const hover = {
  boxShadow: "-2px -2px 0 8px #ECC94B, 6px 6px 0 8px #B83280",
  width: "80",
  height: "80",
  _first: {
    opacity: 1,
    height: "none",
  },
  "&>svg": { opacity: 1, height: "unset" },
  "&>.chakra-stack": { opacity: 1, height: "unset" },
  "&>.css-jpsgiz": {
    width: "32",
    height: "32",
    transition: "width 0.3s, height 0.3s",
  },
};

const active = {
  boxShadow: "-1px -1px 0 4px #ECC94B, 3px 3px 0 4px #B83280",
};

const ProjectCard = (props) => {
  return (
    <Flex
      as="a"
      display="flex"
      borderRadius="6"
      width="80"
      height="80"
      backgroundColor="gray.800"
      _hover={hover}
      justifyContent="space-between"
      py="4"
      px="6"
      direction="column"
      alignItems="flex-end"
      transition="box-shadow 0.3s"
      _active={active}
      overflow="hidden"
      href={props.url}
      target="_blank"
      mt="8"
      {...props}
    >
      <IoMdOpen
        size="3rem"
        color="white"
        opacity="0"
        transition="opacity 0.3s, height 0.3s"
        height="0px"
      ></IoMdOpen>
      <Box width="48" height="48" alignSelf="center">
        {props.svg}
      </Box>
      <VStack
        alignItems="start"
        w="100%"
        opacity="0"
        transition="opacity 0.3s, height 0.3s"
        height="8"
      >
        <Heading color="white" fontSize="3xl">
          {props.title}
        </Heading>
        <Text color="white">{props.description}</Text>
      </VStack>
    </Flex>
  );
};

export default ProjectCard;
