import * as React from "react";
import { Box, ChakraProvider } from "@chakra-ui/react";
import Footer from "../components/footer";
import theme from "../theme/theme";
import { Helmet } from "react-helmet";

// markup
function Layout(props) {
  return (
    <ChakraProvider theme={theme}>
      <Helmet>
        <title>{props.title}</title>
      </Helmet>
      <Box px={["6", "8"]} maxW="1280px" width="100%" m="0 auto">
        {props.children}
        <Footer></Footer>
      </Box>
    </ChakraProvider>
  );
}

export default Layout;
