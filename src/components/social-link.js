import * as React from "react";
import {
  VStack,
  Text,
  Heading,
  Box,
  ChakraProvider,
  keyframes,
  Link,
} from "@chakra-ui/react";
import { Link as gLink } from "gatsby";
import {
  IoLogoTwitter,
  IoLogoGithub,
  IoLogoGitlab,
  IoLogoFacebook,
} from "react-icons/io";

const handler = (social) => {
  switch (social) {
    case "github":
      return <IoLogoGithub size="24px"></IoLogoGithub>;
      break;
    case "twitter":
      return <IoLogoTwitter size="24px"></IoLogoTwitter>;
      break;
    case "gitlab":
      return <IoLogoGitlab size="24px"></IoLogoGitlab>;
      break;
    case "default":
      return <IoLogoFacebook size="24px"></IoLogoFacebook>;
  }
};

const SocialLink = (props) => {
  return (
    <Link href="twitter.com" color="twitter.500">
      {handler(props.social)}
    </Link>
  );
};

export default SocialLink;
